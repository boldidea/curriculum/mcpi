from setuptools import setup, find_packages

setup(
    name='mcpi',
    version='1.11',
    packages=find_packages(),
    long_description=open('README.md').read()
)
